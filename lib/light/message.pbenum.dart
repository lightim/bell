///
//  Generated code. Do not modify.
//  source: message.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' show int, dynamic, String, List, Map;
import 'package:protobuf/protobuf.dart' as $pb;

class RequestType extends $pb.ProtobufEnum {
  static const RequestType Auth = const RequestType._(0, 'Auth');

  static const List<RequestType> values = const <RequestType> [
    Auth,
  ];

  static final Map<int, RequestType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static RequestType valueOf(int value) => _byValue[value];
  static void $checkItem(RequestType v) {
    if (v is! RequestType) $pb.checkItemFailed(v, 'RequestType');
  }

  const RequestType._(int v, String n) : super(v, n);
}

class Version extends $pb.ProtobufEnum {
  static const Version Undefined = const Version._(0, 'Undefined');
  static const Version V1 = const Version._(1, 'V1');

  static const List<Version> values = const <Version> [
    Undefined,
    V1,
  ];

  static final Map<int, Version> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Version valueOf(int value) => _byValue[value];
  static void $checkItem(Version v) {
    if (v is! Version) $pb.checkItemFailed(v, 'Version');
  }

  const Version._(int v, String n) : super(v, n);
}

class AuthResponse_Status extends $pb.ProtobufEnum {
  static const AuthResponse_Status Undefined = const AuthResponse_Status._(0, 'Undefined');
  static const AuthResponse_Status OK = const AuthResponse_Status._(1, 'OK');
  static const AuthResponse_Status AppNotFound = const AuthResponse_Status._(2, 'AppNotFound');
  static const AuthResponse_Status TokenNotFound = const AuthResponse_Status._(3, 'TokenNotFound');

  static const List<AuthResponse_Status> values = const <AuthResponse_Status> [
    Undefined,
    OK,
    AppNotFound,
    TokenNotFound,
  ];

  static final Map<int, AuthResponse_Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static AuthResponse_Status valueOf(int value) => _byValue[value];
  static void $checkItem(AuthResponse_Status v) {
    if (v is! AuthResponse_Status) $pb.checkItemFailed(v, 'AuthResponse_Status');
  }

  const AuthResponse_Status._(int v, String n) : super(v, n);
}

class ContentMessage_Type extends $pb.ProtobufEnum {
  static const ContentMessage_Type Undefined = const ContentMessage_Type._(0, 'Undefined');
  static const ContentMessage_Type Text = const ContentMessage_Type._(1, 'Text');
  static const ContentMessage_Type Image = const ContentMessage_Type._(2, 'Image');
  static const ContentMessage_Type Voice = const ContentMessage_Type._(3, 'Voice');
  static const ContentMessage_Type Video = const ContentMessage_Type._(4, 'Video');
  static const ContentMessage_Type File = const ContentMessage_Type._(5, 'File');

  static const List<ContentMessage_Type> values = const <ContentMessage_Type> [
    Undefined,
    Text,
    Image,
    Voice,
    Video,
    File,
  ];

  static final Map<int, ContentMessage_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ContentMessage_Type valueOf(int value) => _byValue[value];
  static void $checkItem(ContentMessage_Type v) {
    if (v is! ContentMessage_Type) $pb.checkItemFailed(v, 'ContentMessage_Type');
  }

  const ContentMessage_Type._(int v, String n) : super(v, n);
}

class MessageAck_Status extends $pb.ProtobufEnum {
  static const MessageAck_Status Undefined = const MessageAck_Status._(0, 'Undefined');
  static const MessageAck_Status OK = const MessageAck_Status._(1, 'OK');
  static const MessageAck_Status BlackList = const MessageAck_Status._(2, 'BlackList');
  static const MessageAck_Status NotFriend = const MessageAck_Status._(3, 'NotFriend');
  static const MessageAck_Status NotInGroup = const MessageAck_Status._(4, 'NotInGroup');
  static const MessageAck_Status NotInRoom = const MessageAck_Status._(5, 'NotInRoom');

  static const List<MessageAck_Status> values = const <MessageAck_Status> [
    Undefined,
    OK,
    BlackList,
    NotFriend,
    NotInGroup,
    NotInRoom,
  ];

  static final Map<int, MessageAck_Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static MessageAck_Status valueOf(int value) => _byValue[value];
  static void $checkItem(MessageAck_Status v) {
    if (v is! MessageAck_Status) $pb.checkItemFailed(v, 'MessageAck_Status');
  }

  const MessageAck_Status._(int v, String n) : super(v, n);
}

class Notification_Type extends $pb.ProtobufEnum {
  static const Notification_Type Undefined = const Notification_Type._(0, 'Undefined');
  static const Notification_Type KickOff = const Notification_Type._(1, 'KickOff');
  static const Notification_Type FriendRequest = const Notification_Type._(2, 'FriendRequest');
  static const Notification_Type FriendReply = const Notification_Type._(3, 'FriendReply');

  static const List<Notification_Type> values = const <Notification_Type> [
    Undefined,
    KickOff,
    FriendRequest,
    FriendReply,
  ];

  static final Map<int, Notification_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Notification_Type valueOf(int value) => _byValue[value];
  static void $checkItem(Notification_Type v) {
    if (v is! Notification_Type) $pb.checkItemFailed(v, 'Notification_Type');
  }

  const Notification_Type._(int v, String n) : super(v, n);
}

class Message_Type extends $pb.ProtobufEnum {
  static const Message_Type Undefined = const Message_Type._(0, 'Undefined');
  static const Message_Type Ping = const Message_Type._(1, 'Ping');
  static const Message_Type Pong = const Message_Type._(2, 'Pong');
  static const Message_Type Request = const Message_Type._(1001, 'Request');
  static const Message_Type Response = const Message_Type._(1002, 'Response');
  static const Message_Type ContentMessage = const Message_Type._(1003, 'ContentMessage');
  static const Message_Type GroupContentMessage = const Message_Type._(1004, 'GroupContentMessage');
  static const Message_Type RoomContentMessage = const Message_Type._(1005, 'RoomContentMessage');
  static const Message_Type MessageAck = const Message_Type._(1006, 'MessageAck');
  static const Message_Type Notification = const Message_Type._(1007, 'Notification');

  static const List<Message_Type> values = const <Message_Type> [
    Undefined,
    Ping,
    Pong,
    Request,
    Response,
    ContentMessage,
    GroupContentMessage,
    RoomContentMessage,
    MessageAck,
    Notification,
  ];

  static final Map<int, Message_Type> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Message_Type valueOf(int value) => _byValue[value];
  static void $checkItem(Message_Type v) {
    if (v is! Message_Type) $pb.checkItemFailed(v, 'Message_Type');
  }

  const Message_Type._(int v, String n) : super(v, n);
}

