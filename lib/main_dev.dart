import 'package:flutter/material.dart';
import 'package:bell/app.dart';
import 'package:bell/app_config.dart';

void main() {
  AppConfig configuredApp = AppConfig(
    appName: 'Bell',
    apiBaseUrl: 'http://192.168.50.148:1212/api',
    imBaseUrl: 'ws://192.168.50.148:12125/light',
    imAppId: 'qNqoEH4JOAHQ',
    debug: true,
    child: App(),
  );
  runApp(configuredApp);
}
