import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:bell/notification/local.dart';
import 'package:bell/screen/welcome_screen.dart';
import 'package:bell/i18n.dart';

class App extends StatefulWidget {
  AppState createState() => AppState();
}

class AppState extends State<App> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    /* 在app进入前台的时候关闭推送 */
    if (state != AppLifecycleState.resumed) {
      localNotificationEnabled = true;
    } else {
      localNotificationEnabled = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bell',
      localizationsDelegates: [
        const I18NDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('zh', 'CN'), // Chinese
      ],
      color: Colors.lightBlue,
      home: WelcomeScreen(),
    );
  }
}
