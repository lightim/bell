import 'package:flutter/material.dart';
import 'package:bell/notification/local.dart';
import 'package:bell/platform/android.dart';
import 'package:bell/screen/chat_fragment.dart';
import 'package:bell/screen/contact_fragment.dart';
import 'package:bell/screen/profile_fragment.dart';
import 'package:bell/api/account.dart';
import 'package:bell/screen/search_friend_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  int pageIndex = 0;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
    initLocalNotification(onSelectNotification);

    AccountService.getAccount();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onWillPop,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Bell'),
            actions: <Widget>[
              PopupMenuButton(
                onSelected: onMenuSelected,
                itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                      PopupMenuItem(
                        value: 0,
                        child: Text('发起群聊'),
                      ),
                      PopupMenuItem(
                        value: 1,
                        child: Text('添加朋友'),
                      ),
                      PopupMenuItem(
                        value: 2,
                        child: Text('帮助与反馈'),
                      ),
                    ],
              )
            ],
          ),
          body: PageView.builder(
            controller: pageController,
            onPageChanged: (i) {
              this.setState(() {
                pageIndex = i;
              });
            },
            itemCount: 3,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return ChatFragment();
              } else if (index == 1) {
                return ContactFragment();
              }
              return ProfileFragment();
            },
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: pageIndex,
            onTap: (i) {
              pageController.animateToPage(i,
                  duration: Duration(milliseconds: 200),
                  curve: Curves.easeInOut);
            },
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.chat_bubble), title: Text('聊天')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.people), title: Text('通讯录')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle), title: Text('我')),
            ],
          ),
        ));
  }

  Future<bool> onWillPop() async {
    final r = await AndroidChannel.moveToBack();
    return !r;
  }

  Future onSelectNotification(String payload) async {
    if (payload == null || payload.length == 0) {
      return;
    }
    debugPrint('notification payload: ' + payload);
  }

  onMenuSelected(t) {
    if (t == 1) {
      onAddFriendSelected();
    }
  }

  onAddFriendSelected() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchFriendScreen()));
  }
}
