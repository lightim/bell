import 'package:flutter/material.dart';
import 'package:bell/screen/main_screen.dart';
import 'package:bell/screen/login_screen.dart';
import 'package:bell/api/api.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreenState createState() => WelcomeScreenState();
}

class WelcomeScreenState extends State<WelcomeScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 1500)).then((v) async {
      var token = await getToken();
      if (token != '') {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainScreen()));
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox.expand(
      child: Image.asset(
        'assets/images/welcome.jpg',
        fit: BoxFit.cover,
      ),
    ));
  }
}
