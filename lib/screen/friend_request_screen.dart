import 'package:flutter/material.dart';
import 'package:bell/screen/search_friend_screen.dart';
import 'package:bell/widget/friend_request_item.dart';
import 'package:bell/api/friend.dart';
import 'package:bell/db/friend.dart';

class FriendRequestScreen extends StatefulWidget {
  FriendRequestScreen({Key key}) : super(key: key);

  FriendRequestScreenState createState() => FriendRequestScreenState();
}

class FriendRequestScreenState extends State<FriendRequestScreen> {
  List<FriendRequest> requests = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('新的朋友'),
        actions: <Widget>[
          FlatButton(
            onPressed: onSearchFriend,
            child: Text(
              '添加联系人',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 8, right: 8),
        color: Colors.white,
        child: ListView.separated(
          padding: EdgeInsets.all(0),
          itemCount: this.requests.length,
          separatorBuilder: (BuildContext context, int index) {
            return Container(
                padding: EdgeInsets.only(left: 76),
                child: Divider(height: 0, color: Colors.grey));
          },
          itemBuilder: (BuildContext context, int index) {
            var req = this.requests[index];
            return FriendRequestItem(
                id: req.id,
                message: req.message,
                userID: req.userID,
                status: req.status);
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    FriendDatabase.FindFriendRequests().then((list) {
      this.requests = list;
      this.setState(() {});
    }).catchError((e) {
      print(e);
    });
  }

  onSearchFriend() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchFriendScreen()));
  }
}
