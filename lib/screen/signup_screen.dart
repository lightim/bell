import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:bell/screen/main_screen.dart';
import 'package:bell/api/api.dart';
import 'package:bell/api/file.dart';
import 'package:bell/api/account.dart';

class SignupScreen extends StatefulWidget {
  SignupScreenState createState() => SignupScreenState();
}

class SignupScreenState extends State<SignupScreen> {
  ImageProvider avatarProvider = AssetImage('assets/images/account_circle.png');
  File avatarFile;

  String emailError;
  String nicknameError;
  String passwordError;

  String nickname = '';
  String email = '';
  String password = '';
  String avatar = '';
  bool loading = false;

  GlobalKey signupButtonKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  onNicknameChanged(String text) {
    setState(() {
      nickname = text;
      nicknameError = null;
    });
  }

  onEmailChanged(String text) {
    setState(() {
      email = text;
      emailError = null;
    });
  }

  onPasswordChanged(String text) {
    setState(() {
      password = text;
      passwordError = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: Theme.of(context).accentColor,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(24.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '注册邮箱帐号',
                    style: TextStyle(fontSize: 28),
                  )),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                      child: TextField(
                    onChanged: onNicknameChanged,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(20),
                    ],
                    decoration: InputDecoration(
                        labelText: '昵称', errorText: nicknameError),
                  )),
                  Container(
                      padding: EdgeInsets.only(left: 8, bottom: 4),
                      child: InkWell(
                          onTap: onImagePressed,
                          child: Image(
                              width: 64, height: 64, image: avatarProvider)))
                ],
              ),
              TextField(
                onChanged: onEmailChanged,
                decoration:
                    InputDecoration(labelText: '邮箱', errorText: emailError),
                inputFormatters: [
                  LengthLimitingTextInputFormatter(32),
                ],
              ),
              TextField(
                onChanged: onPasswordChanged,
                decoration:
                    InputDecoration(labelText: '密码', errorText: passwordError),
                obscureText: true,
              ),
              Container(
                  margin: EdgeInsets.only(top: 48),
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    key: signupButtonKey,
                    onPressed: (loading ||
                            nickname.length == 0 ||
                            email.length == 0 ||
                            password.length == 0 ||
                            avatar.length == 0)
                        ? null
                        : onSignupPressed,
                    textTheme: ButtonTextTheme.primary,
                    padding: EdgeInsets.all(9),
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    disabledColor: Theme.of(context).primaryColorLight,
                    disabledTextColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      '注册',
                      style: TextStyle(fontSize: 20),
                    ),
                  ))
            ],
          )),
    );
  }

  onImagePressed() async {
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file == null) {
      return;
    }
    var croppedFile = await ImageCropper.cropImage(
      sourcePath: file.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 128,
      maxHeight: 128,
      toolbarColor: Theme.of(context).primaryColor,
    );
    if (croppedFile == null) {
      return;
    }
    String fileid = '';
    try {
      fileid = await FileService.upload(croppedFile);
    } catch (e) {
      Scaffold.of(signupButtonKey.currentContext).showSnackBar(SnackBar(
        content: Text('上传图片失败'),
      ));
      return;
    }
    avatarFile = croppedFile;
    avatar = fileid;
    setState(() {
      avatarProvider = FileImage(avatarFile);
    });
  }

  onSignupPressed() async {
    Scaffold.of(signupButtonKey.currentContext).removeCurrentSnackBar();
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      loading = true;
    });
    Token token;
    try {
      token = await AccountService.signup(email, password, avatar, nickname);
    } on ApiException catch (e) {
      if (e.status == 10001) {
        emailError = '邮箱已存在，请直接登录';
      } else if (e.status == 10005) {
        emailError = '邮箱格式有误';
      } else {
        print(e.status);
        Scaffold.of(signupButtonKey.currentContext).showSnackBar(SnackBar(
          content: Text('未知错误'),
        ));
      }
    }
    loading = false;
    setState(() {});
    if (token == null) {
      return;
    }

    await setToken(token.token, token.userID);
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => MainScreen()),
        (Route<dynamic> route) => false);
  }
}
