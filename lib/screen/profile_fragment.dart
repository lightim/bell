import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bell/screen/setting_screen.dart';
import 'package:bell/widget/rounded.dart';
import 'package:bell/widget/icon_text_bar.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/file.dart';

class ProfileHeader extends StatelessWidget {
  final Account account;

  ProfileHeader({Key key, @required this.account}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
            onTap: onHeaderPressed,
            child: Container(
              padding:
                  EdgeInsets.only(top: 30, left: 16, right: 16, bottom: 30),
              child: Row(
                children: <Widget>[
                  Rounded(
                    child: Image(
                      width: 76,
                      height: 76,
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(account != null
                          ? FileService.fileUrl(account.avatar)
                          : ''),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.only(left: 12, right: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Text(
                              account != null ? account.nickname : '',
                              style: TextStyle(fontSize: 24),
                              // overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              maxLines: 1,
                            )),
                        Container(
                            child: Text(
                          '贝尔号 : ' + (account != null ? account.username : ''),
                          // overflow: TextOverflow.ellipsis,
                          softWrap: false,
                          maxLines: 1,
                        ))
                      ],
                    ),
                  )),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                    size: 16,
                  )
                ],
              ),
            )));
  }

  onHeaderPressed() {}
}

class ProfileFragment extends StatefulWidget {
  @override
  ProfileFragmentState createState() => ProfileFragmentState();
}

class ProfileFragmentState extends State<ProfileFragment>
    with AutomaticKeepAliveClientMixin<ProfileFragment> {
  @override
  bool get wantKeepAlive => true;

  Account account;

  @override
  void initState() {
    super.initState();

    AccountService.getCachedAccount().then((a) {
      setState(() {
        account = a;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ProfileHeader(account: account),
        Divider(height: 1),
        SizedBox(height: 20),
        Divider(height: 1),
        IconTextBar(
          icon: Icons.settings,
          iconColor: Colors.lightBlue,
          text: '设置',
          onTap: onSettingPressed,
        ),
        Divider(height: 1),
      ],
    ));
  }

  onSettingPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SettingScreen()));
  }
}
