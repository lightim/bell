import 'package:flutter/material.dart';
import 'package:bell/screen/user_profile_screen.dart';
import 'package:bell/api/account.dart';

class SearchFriendScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SearchFriendScreenState();
}

class SearchFriendScreenState extends State<SearchFriendScreen> {
  Account self;
  bool notFound = false;
  bool loading = false;

  @override
  void initState() {
    super.initState();

    AccountService.getCachedAccount().then((v) {
      setState(() {
        self = v;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        '添加朋友',
      )),
      body: Container(
        color: Colors.grey[200],
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  hintText: '贝尔号/邮箱',
                ),
                textInputAction: TextInputAction.search,
                onSubmitted: onSearchSubmmited,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                '我的贝尔号：' + (self != null ? self.username : ''),
                style: TextStyle(color: Colors.grey),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                color: Colors.white,
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(top: 20),
                child: notFound ? Text('该用户不存在') : null,
              ),
            )
          ],
        ),
      ),
    );
  }

  onSearchSubmmited(String text) async {
    if (this.loading || text == null || text.length == 0) {
      return;
    }
    this.loading = true;
    Account account;
    try {
      var accounts = await AccountService.searchAccount(text);
      if (accounts.length > 0) {
        account = accounts[0];
      }
    } catch (e) {
      print(e);
    } finally {
      this.loading = false;
    }
    notFound = account == null;
    setState(() {});
    if (account != null) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UserProfileScreen(
                    id: account.id,
                    source: '搜索',
                  )));
    }
  }
}
