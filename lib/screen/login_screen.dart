import 'package:flutter/material.dart';
import 'package:bell/screen/signup_screen.dart';
import 'package:bell/screen/main_screen.dart';
import 'package:bell/i18n.dart';
import 'package:bell/api/api.dart';
import 'package:bell/api/account.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin<LoginScreen> {
  String username = '';
  String password = '';

  String usernameError;
  String passwordError;

  bool loading = false;
  GlobalKey loginButtonKey = GlobalKey();

  @override
  initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 80, bottom: 20),
          margin: MediaQuery.of(context).padding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                'assets/images/avatar.png',
                width: 96,
                height: 96,
              ),
              Container(
                  margin: EdgeInsets.only(top: 40),
                  child: TextField(
                    keyboardType: TextInputType.emailAddress,
                    onChanged: onPhoneChanged,
                    decoration: InputDecoration(
                        labelText: I18N.of(context).i('email/username'),
                        errorText: usernameError),
                  )),
              Container(
                  child: TextField(
                      obscureText: true,
                      onChanged: onPasswordChanged,
                      decoration: InputDecoration(
                          labelText: I18N.of(context).i('password'),
                          errorText: passwordError))),
              Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.only(top: 48),
                  child: RaisedButton(
                    key: loginButtonKey,
                    onPressed: (loading ||
                            username.length == 0 ||
                            password.length == 0)
                        ? null
                        : this.onLoginPressed,
                    textTheme: ButtonTextTheme.primary,
                    padding: EdgeInsets.all(9),
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    disabledColor: Theme.of(context).primaryColorLight,
                    disabledTextColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      I18N.of(context).i('login'),
                      style: TextStyle(fontSize: 20),
                    ),
                  )),
              Container(
                margin: EdgeInsets.only(top: 12),
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: onSignupPressed,
                    child: Text(
                      I18N.of(context).i('no account'),
                      style: TextStyle(fontSize: 16),
                    )),
              ),
              Expanded(
                child: Container(),
              ),
              Container(
                alignment: Alignment.center,
                child: GestureDetector(
                    onTap: () {},
                    child: Text(
                      I18N.of(context).i('forget password?'),
                      style: TextStyle(fontSize: 16),
                    )),
              )
            ],
          )),
    );
  }

  onPhoneChanged(text) {
    setState(() {
      usernameError = null;
      username = text;
    });
  }

  onPasswordChanged(text) {
    setState(() {
      passwordError = null;
      password = text;
    });
  }

  onLoginPressed() async {
    doLogin(this.username, this.password);
  }

  onSignupPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SignupScreen()));
  }

  doLogin(String username, String password) async {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      loading = true;
    });
    Scaffold.of(loginButtonKey.currentContext).removeCurrentSnackBar();

    Token token;
    try {
      token = await AccountService.login(username, password);
    } on ApiException catch (e) {
      if (e.status == 10003) {
        usernameError = I18N.of(context).i('account not found');
      } else if (e.status == 10004) {
        passwordError = '密码错误';
      } else {
        Scaffold.of(loginButtonKey.currentContext).showSnackBar(SnackBar(
          content: Text('未知错误!'),
        ));
      }
    } catch (e) {
      print(e);
      Scaffold.of(loginButtonKey.currentContext).showSnackBar(SnackBar(
        content: Text('网络错误!'),
      ));
    }
    loading = false;
    setState(() {});
    if (token == null) {
      return;
    }

    await setToken(token.token, token.userID);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MainScreen()));
  }
}
