import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:bell/light/message.pb.dart' as pb;
import 'package:bell/i18n.dart';
import 'package:bell/screen/login_screen.dart';
import 'package:bell/widget/session_item.dart';
import 'package:bell/widget/im_state_bar.dart';
import 'package:bell/notification/local.dart';
import 'package:bell/light/client.dart';
import 'package:bell/api/friend.dart';
import 'package:bell/api/api.dart';
import 'package:bell/api/im.dart';
import 'package:bell/db/friend.dart';
import 'package:bell/db/session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatFragment extends StatefulWidget {
  @override
  ChatFragmentState createState() => ChatFragmentState();
}

class ChatFragmentState extends State<ChatFragment>
    with AutomaticKeepAliveClientMixin<ChatFragment>, TickerProviderStateMixin {
  IMState imState = IMState.Connected;
  bool needReconnect = true;
  List<Session> sessions = [];

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    IMManager.addonopen(onopen);
    IMManager.addonclose(onclose);
    IMManager.addonerror(onerror);
    IMManager.addonreconnect(onreconnect);
    IMManager.addonmessage(onmessage);

    imConnect();

    SessionDatabase.FindSessions().then((sessions) {
      this.sessions = sessions;
      this.setState(() {});
    });
  }

  imConnect() async {
    var token = await getToken();
    if (token == '') {
      showAuthErrorDialog();
    } else {
      IMManager.connect(token);
    }
  }

  @override
  void dispose() {
    IMManager.rmonopen(onopen);
    IMManager.rmonclose(onclose);
    IMManager.rmonerror(onerror);
    IMManager.rmonreconnect(onreconnect);
    IMManager.rmonmessage(onmessage);
    super.dispose();
  }

  Future<void> onopen() async {
    print('open');
    setState(() {
      imState = IMState.Connected;
    });
    // showLocalNotification(0, '链接状态', '建立成功', '');
  }

  Future<void> onmessage(pb.Message m) async {
    print(m.type);
    if (m.type == pb.Message_Type.Notification && m.notification != null) {
      await onnotification(m);
    } else if (m.type == pb.Message_Type.ContentMessage &&
        m.contentMessage != null) {
      await oncontentmessage(m);
    }
  }

  Future<void> onnotification(pb.Message m) async {
    var n = m.notification;
    print(n);

    if (n.type == pb.Notification_Type.FriendRequest) {
      /* 好友请求 */
      showLocalNotification(0, '好友请求',
          n.content['from_nickname'] + ' 给您发送了好友请求', n.content['message']);
      FriendService.GetFriendRequest(int.parse(n.content['id'])).then((r) {
        FriendDatabase.InsertFriendRequest(r);
        SharedPreferences.getInstance().then((prefs) {
          var c = prefs.getInt("newFriendRequest");
          if (c == null) {
            c = 0;
          }
          prefs.setInt("newFriendRequest", c + 1);
        });
      }).catchError((e) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('获取好友请求失败'),
        ));
      });
    }

    if (n.needAck) {
      var r = pb.Message();
      r.id = Uuid().v4();
      r.version = pb.Version.V1;
      r.type = pb.Message_Type.MessageAck;
      r.messageAck = pb.MessageAck();
      r.messageAck.status = pb.MessageAck_Status.OK;
      r.messageAck.msgId = m.id;
      IMManager.send(r);
    }
  }

  Future<void> oncontentmessage(pb.Message m) async {
    print(m);
    /* 发送ACK */
    var r = pb.Message();
    r.id = Uuid().v4();
    r.version = pb.Version.V1;
    r.type = pb.Message_Type.MessageAck;
    r.messageAck = pb.MessageAck();
    r.messageAck.status = pb.MessageAck_Status.OK;
    r.messageAck.msgId = m.id;
    IMManager.send(r);
  }

  Future<void> onclose() async {
    print('close');
    setState(() {
      imState = IMState.Closed;
    });
    Future.delayed(Duration(seconds: 10)).then((v) {
      if (imState == IMState.Closed && this.mounted && needReconnect) {
        setState(() {
          imState = IMState.Reconnecting;
          imConnect();
        });
      }
    });
    // showLocalNotification(0, '链接状态', '关闭', '');
  }

  Future<void> onreconnect() async {
    print('reconnect');
    setState(() {
      imState = IMState.Reconnecting;
    });
  }

  Future<void> onerror(errcode) async {
    if (errcode == IMErrorCode.AuthFailed) {
      IMManager.disconnect();
      showAuthErrorDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
        child: ListView.separated(
      itemCount: this.sessions.length + 1,
      padding: EdgeInsets.all(0),
      separatorBuilder: (BuildContext context, int index) {
        if (index < 1) {
          return Container();
        }
        return Container(
            padding: EdgeInsets.only(left: 76),
            child: Divider(height: 1, color: Colors.grey));
      },
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return AnimatedSize(
              curve: Curves.bounceInOut,
              duration: Duration(milliseconds: 150),
              vsync: this,
              child: IMStateBar(
                imState: imState,
                onRetry: onNetworkRetry,
              ));
        }
        var session = this.sessions[index - 1];
        return SessionItem(
          session: session,
        );
      },
    ));
  }

  onNetworkRetry() {
    if (imState != IMState.Closed) {
      return;
    }
    setState(() {
      imState = IMState.Reconnecting;
      imConnect();
    });
  }

  showAuthErrorDialog() {
    needReconnect = false;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(I18N.of(context).i('error')),
            content: Text(I18N.of(context).i('auth error')),
            actions: <Widget>[
              FlatButton(
                child: Text(I18N.of(context).i('yes')),
                onPressed: () {
                  clearToken();
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                      (Route<dynamic> route) => false);
                },
              )
            ],
          );
        });
  }
}
