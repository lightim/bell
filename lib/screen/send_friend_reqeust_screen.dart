import 'package:flutter/material.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/friend.dart';

/*
 * 发送好友请求界面
 */

class SendFriendRequestScreen extends StatefulWidget {
  User user;
  SendFriendRequestScreen({@required this.user, Key key}) : super(key: key);

  @override
  SendFriendRequestScreenState createState() => SendFriendRequestScreenState();
}

class SendFriendRequestScreenState extends State<SendFriendRequestScreen> {
  Account self;
  User user;

  TextEditingController messageController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  FocusNode messageFocusNode = FocusNode();
  FocusNode remarkFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    AccountService.getCachedAccount().then((a) {
      this.self = a;
      this.messageController.text = '我是' + a.nickname;
      this.setState(() {});
    }).catchError((e) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('未知错误'),
      ));
    });
    this.user = this.widget.user;
    this.remarkController.text = this.user.nickname;

    this.messageController.addListener(() {
      this.setState(() {});
    });
    this.remarkController.addListener(() {
      this.setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('好友请求'),
        actions: <Widget>[
          Container(
            child: FlatButton(
              child: Text(
                '发送',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: onSend,
            ),
          )
        ],
      ),
      body: this.self != null
          ? Container(
              color: Colors.grey[200],
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(
                      left: 12,
                      right: 12,
                      top: 12,
                      bottom: 12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '你需要发送验证申请，等待对方通过',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextField(
                          decoration: InputDecoration(
                            hintText: '验证信息',
                            suffixIcon: this.messageController.text != "" &&
                                    messageFocusNode.hasFocus
                                ? IconButton(
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      this.messageController.clear();
                                    },
                                  )
                                : null,
                          ),
                          focusNode: messageFocusNode,
                          controller: this.messageController,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(
                      left: 12,
                      right: 12,
                      top: 12,
                      bottom: 12,
                    ),
                    margin: EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '为朋友设置备注',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextField(
                          style: !remarkFocusNode.hasFocus &&
                                  remarkController.text == this.user.nickname
                              ? TextStyle(color: Colors.grey)
                              : null,
                          decoration: InputDecoration(
                            hintText: '备注',
                            suffixIcon: this.remarkController.text != "" &&
                                    remarkFocusNode.hasFocus
                                ? IconButton(
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      this.remarkController.clear();
                                    },
                                  )
                                : null,
                          ),
                          controller: this.remarkController,
                          focusNode: remarkFocusNode,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          : null,
    );
  }

  onSend() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: Dialog(
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(6),
          width: 100,
          height: 44,
          child: Text('正在发送...'),
        ),
      ),
    );

    try {
      await FriendService.CreateFriendRequest(
          this.user.id,
          this.messageController.text,
          this.remarkController.text == this.user.nickname
              ? ''
              : this.remarkController.text);
    } catch (e) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('网络错误'),
      ));
    } finally {
      Navigator.pop(context);
    }

    Navigator.pop(context);
  }
}
