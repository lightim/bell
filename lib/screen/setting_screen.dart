import 'package:flutter/material.dart';
import 'package:bell/api/im.dart';
import 'package:bell/api/api.dart';
import 'package:bell/widget/icon_text_bar.dart';
import 'package:bell/screen/login_screen.dart';
import 'package:bell/db/sqlite3.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('设置'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            IconTextBar(text: '帐号与安全'),
            Divider(height: 1),
            SizedBox(height: 15),
            Divider(height: 1),
            IconTextBar(text: '新消息提醒'),
            IconTextDivider(left: 16),
            IconTextBar(text: '聊天'),
            IconTextDivider(left: 16),
            IconTextBar(text: '隐私'),
            IconTextDivider(left: 16),
            IconTextBar(text: '通用'),
            Divider(height: 1),
            SizedBox(height: 15),
            Divider(height: 1),
            IconTextBar(text: '关于贝尔'),
            IconTextDivider(left: 16),
            IconTextBar(text: '帮助与反馈'),
            Divider(height: 1),
            SizedBox(height: 15),
            Divider(height: 1),
            IconTextBar(
              text: '退出',
              onTap: () => onLogout(context),
            ),
            Divider(height: 1),
          ],
        ),
      ),
    );
  }

  onLogout(BuildContext context) async {
    IMManager.disconnect();
    await clearToken();
    await clearDB();

    await Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
        (Route<dynamic> route) => false);
  }
}
