import 'package:flutter/material.dart';
import 'package:bell/api/im.dart';
import 'package:bell/api/friend.dart';
import 'package:bell/db/user.dart';
import 'package:bell/light/message.pb.dart' as pb;
import 'package:bell/notification/local.dart';
import 'package:bell/screen/friend_request_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bell/widget/icon_text_bar.dart';
import 'package:bell/widget/friend_item.dart';

class ContactFragment extends StatefulWidget {
  @override
  ContactFragmentState createState() => ContactFragmentState();
}

class ContactFragmentState extends State<ContactFragment>
    with AutomaticKeepAliveClientMixin<ContactFragment> {
  @override
  bool get wantKeepAlive => true;
  int newFriendRequestCount = 0;
  List<UserFriend> friends = [];

  @override
  void initState() {
    super.initState();
    IMManager.addonmessage(onmessage);

    SharedPreferences.getInstance().then((prefs) {
      var c = prefs.getInt("newFriendRequest");
      if (c != null) {
        this.newFriendRequestCount = c;
      }
      this.setState(() {});
    });

    FriendService.FindUserFriends().then((friends) {
      this.friends = friends;
      for (var i = 0; i < 100; i++) {
        this.friends.add(friends[i]);
      }
      this.setState(() {});
      for (var i = 0; i < friends.length; i++) {
        UserDatabase.InsertUser(friends[i].friend);
      }
    }).catchError((e) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('获取好友列表失败'),
      ));
    });
  }

  @override
  void dispose() {
    IMManager.rmonmessage(onmessage);
    super.dispose();
  }

  Future<void> onmessage(pb.Message m) async {
    if (m.type == pb.Message_Type.Notification && m.notification != null) {
      var n = m.notification;
      if (n.type == pb.Notification_Type.FriendRequest) {
        this.setState(() {
          this.newFriendRequestCount = this.newFriendRequestCount + 1;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          IconTextBar(
            icon: Icons.supervised_user_circle,
            iconColor: Colors.orangeAccent,
            text: '新的朋友',
            onTap: onNewFriendPressed,
            badge: newFriendRequestCount,
          ),
          IconTextDivider(),
          IconTextBar(
            icon: Icons.group_work,
            iconColor: Colors.green,
            text: '群聊',
            onTap: onGroupPressed,
          ),
          Divider(height: 1),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.all(0),
              itemCount: this.friends.length,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                    padding: EdgeInsets.only(left: 76),
                    child: Divider(height: 0, color: Colors.grey));
              },
              itemBuilder: (BuildContext context, int index) {
                var f = this.friends[index];
                return FriendItem(
                  userID: f.friendID,
                  avatar: f.friend.avatar,
                  nickname: f.friend.nickname,
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  onNewFriendPressed() {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt("newFriendRequest", 0);
      this.setState(() {
        this.newFriendRequestCount = 0;
      });
    });
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => FriendRequestScreen()));
  }

  onGroupPressed() {}
}
