import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/file.dart';
import 'package:bell/widget/rounded.dart';
import 'package:bell/screen/person_chat_screen.dart';
import 'package:bell/screen/send_friend_reqeust_screen.dart';

class UserProfileScreen extends StatefulWidget {
  int id;
  String source;

  UserProfileScreen({Key key, @required this.id, this.source})
      : super(key: key);

  UserProfileScreenState createState() => UserProfileScreenState();
}

class UserProfileScreenState extends State<UserProfileScreen> {
  User user;
  @override
  void initState() {
    super.initState();
    AccountService.getUser(this.widget.id).then((a) {
      if (a == null) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('用户不存在'),
        ));
      } else {
        this.user = a;
        this.setState(() {});
      }
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('用户信息'),
      ),
      body: Container(
        color: Colors.grey[200],
        child: this.user != null
            ? Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(
                        left: 16, right: 16, top: 16, bottom: 20),
                    child: Row(
                      children: <Widget>[
                        Rounded(
                          child: Image(
                            width: 64,
                            height: 64,
                            fit: BoxFit.cover,
                            image: this.user != null
                                ? CachedNetworkImageProvider(
                                    FileService.fileUrl(this.user.avatar))
                                : AssetImage('assets/images/avatar.png'),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 4, bottom: 4),
                                child: Text(
                                  this.user != null ? this.user.nickname : '',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Text('所在地: 上海 徐汇')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.grey[400],
                    height: 0,
                  ),
                  Material(
                    color: Colors.white,
                    child: InkWell(
                      onTap: onUserEdit,
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 16, right: 16, top: 16, bottom: 16),
                        child: Row(
                          children: <Widget>[
                            Text(
                              '设置备注和标签',
                              style: TextStyle(fontSize: 16),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  this.widget.source != ""
                      ? Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: () {},
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 16, right: 16, top: 16, bottom: 16),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 80,
                                      child: Text(
                                        '来源',
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        this.widget.source,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      : null,
                  this.user.relationship != null &&
                          this.user.relationship.isFriend
                      ? Material(
                          color: Colors.white,
                          child: InkWell(
                            onTap: onMore,
                            child: Container(
                              padding: EdgeInsets.only(
                                  left: 16, right: 16, top: 16, bottom: 16),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    '更多',
                                    style: TextStyle(fontSize: 16),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      : null,
                  this.user.relationship == null ||
                          !this.user.relationship.isFriend
                      ? Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: onAdd,
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 16, right: 16, top: 16, bottom: 16),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '添加',
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          ),
                        )
                      : null,
                  this.user.relationship != null &&
                          this.user.relationship.isFriend
                      ? Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: this.onSendMessage,
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 16, right: 16, top: 16, bottom: 16),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '发送消息',
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          ),
                        )
                      : null,
                ].where((c) => c != null).toList(),
              )
            : null,
      ),
    );
  }

  onUserEdit() {
    print('1111');
  }

  onAdd() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SendFriendRequestScreen(user: this.user)));
  }

  onSendMessage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonChatScreen(
                  userID: 1,
                )));
  }

  onMore() {}
}
