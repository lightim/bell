import 'package:flutter/material.dart';
import 'package:bell/widget/chat_input.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/api.dart';
import 'package:bell/db/user.dart';
import 'package:bell/db/session.dart';
import 'package:bell/db/message.dart';
import 'package:bell/api/im.dart';
import 'package:uuid/uuid.dart';
import 'package:bell/light/message.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

class PersonChatScreen extends StatefulWidget {
  int userID;
  PersonChatScreen({@required this.userID, Key key}) : super(key: key);
  @override
  PersonChatScreenState createState() => PersonChatScreenState();
}

class PersonChatScreenState extends State<PersonChatScreen>
    with TickerProviderStateMixin<PersonChatScreen> {
  User user;
  int selfID;

  List<Message> messages = [];

  @override
  void initState() {
    super.initState();

    UserDatabase.FetchUser(this.widget.userID).then((u) {
      if (u != null) {
        this.user = u;
        MessageDatabase.FindPersonMessage(u.id, DateTime.now(), 50)
            .then((messages) {
          this.messages = messages;
          this.setState(() {});
        });
        this.setState(() {});
      }
    }).catchError((e) {
      print(e);
    });

    getUserID().then((id) {
      this.selfID = id;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onWillPop,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: IconButton(
              onPressed: onBackPressed,
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text(this.user != null ? this.user.nickname : ''),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: () {},
              )
            ],
          ),
          body: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.grey[200],
                    alignment: Alignment.center,
                    child: Text('hello world'),
                  ),
                ),
                ChatInput(
                  onSubmit: onSubmit,
                ),
              ],
            ),
          ),
        ));
  }

  onSubmit(String text) {
    var pm = pb.Message();
    pm.version = pb.Version.V1;
    pm.type = pb.Message_Type.ContentMessage;
    pm.id = Uuid().v4();
    pm.fromUserId = this.selfID.toString();
    pm.toUserId = this.user.id.toString();
    pm.contentMessage = pb.ContentMessage();
    pm.contentMessage.type = pb.ContentMessage_Type.Text;
    pm.contentMessage.content['text'] = text;
    pm.ctime = Int64(DateTime.now().millisecondsSinceEpoch * 1000000);
    IMManager.send(pm);
    SessionDatabase.InsertSession(SessionType.Person, this.user.id);
    var m = Message.fromPB(pm, 0);
    MessageDatabase.InsertMessage(m);
    this.messages.add(m);
    this.setState(() {});
  }

  /* 从聊天界面退回到主界面 */
  Future<bool> onWillPop() async {
    Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
    return false;
  }

  onBackPressed() {
    Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
  }
}
