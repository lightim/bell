import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class I18N {
  I18N(this.locale);

  final Locale locale;

  static I18N of(BuildContext context) {
    return Localizations.of<I18N>(context, I18N);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'zh': {
      'login': '登录',
      'signup': '注册',
      'forget password?': '忘记密码？',
      'email/username': '邮箱/用户名',
      'password': '密码',
      'no account': '没有帐号？注册',
      'error': '错误',
      'auth error': '用户认证出错，请重新登录.',
      'yes': '确认',
      'account not found': '账号不存在'
    },
    'en': {
      'login': 'Login',
      'signup': 'Signup',
      'forget password?': 'Forget Password?',
      'email/username': 'Email/Username',
      'password': 'Password',
      'no account': 'No Account? Signup.',
      'error': 'Error',
      'auth error': 'Authorization failure, please re-login.',
      'yes': 'Yes',
      'account not found': 'Invalid Account'
    },
  };

  String i(String name) {
    return _localizedValues[locale.languageCode][name];
  }
}

class I18NDelegate extends LocalizationsDelegate<I18N> {
  const I18NDelegate();

  @override
  bool isSupported(Locale locale) =>
      ['zh', 'pt', 'en'].contains(locale.languageCode);

  @override
  Future<I18N> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<I18N>(I18N(locale));
  }

  @override
  bool shouldReload(I18NDelegate old) => false;
}
