import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

class AppConfig extends InheritedWidget {
  final String appName;
  final String apiBaseUrl;
  final String imBaseUrl;
  final String imAppId;
  final bool debug;

  static AppConfig instance;

  AppConfig(
      {@required this.appName,
      @required this.apiBaseUrl,
      @required this.imBaseUrl,
      @required this.debug,
      @required this.imAppId,
      @required Widget child})
      : super(child: child) {
    instance = this;
  }

  static AppConfig of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
