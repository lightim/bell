import 'package:flutter/material.dart';
import 'package:bell/widget/rounded.dart';
import 'package:bell/api/file.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/friend.dart';
import 'package:bell/db/friend.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bell/screen/person_chat_screen.dart';

class FriendItem extends StatelessWidget {
  final int userID;
  final String avatar;
  final String nickname;

  FriendItem({this.userID, this.avatar, this.nickname, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PersonChatScreen(
                        userID: this.userID,
                      )));
        },
        child: Container(
          padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
          child: Row(
            children: <Widget>[
              Rounded(
                child: Image(
                  width: 34,
                  height: 34,
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(
                      FileService.fileUrl(this.avatar)),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 16),
                  child: Text(this.nickname)),
            ],
          ),
        ),
      ),
    );
  }
}
