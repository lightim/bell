import 'package:flutter/material.dart';

class ChatActionPanel extends StatefulWidget {
  final bool visible;
  final double height;

  ChatActionPanel({Key key, @required this.visible, @required this.height})
      : super(key: key);

  @override
  ChatActionPanelState createState() => ChatActionPanelState();
}

class ChatActionPanelState extends State<ChatActionPanel> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      height: widget.visible ? widget.height : 0,
    );
  }
}
