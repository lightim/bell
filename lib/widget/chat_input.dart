import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bell/widget/chat_action_panel.dart';
import 'package:bell/widget/audio_input.dart';

enum SubmitAction {
  Add,
  Send,
}

enum InputAction {
  Text,
  Audio,
}

class ChatInput extends StatefulWidget {
  final ValueChanged<String> onSubmit;

  ChatInput({Key key, @required this.onSubmit}) : super(key: key);

  @override
  ChatInputState createState() => ChatInputState();
}

class ChatInputState extends State<ChatInput> {
  TextEditingController inputTextController;
  InputAction inputAction = InputAction.Text;
  SubmitAction submitAction = SubmitAction.Add;

  bool actionVisible = false;

  KeyboardVisibilityNotification keyboardVisibilityNotification;
  bool keyboardVisible = false;
  double keyboardHeight = 250;
  int keyboardListenerId = 0;

  FocusNode textFocusNode;

  @override
  void initState() {
    super.initState();
    inputTextController = TextEditingController();
    textFocusNode = FocusNode();
    textFocusNode.addListener(() {
      if (textFocusNode.hasFocus) {
        var text = inputTextController.text;
        updateSubmitAction(text);
      }
    });

    keyboardVisibilityNotification = KeyboardVisibilityNotification();
    keyboardListenerId = keyboardVisibilityNotification.addNewListener(
      onChange: (bool visible) {
        setState(() {
          keyboardVisible = visible;
          if (visible) {
            actionVisible = false;
          }
        });
      },
    );

    SharedPreferences.getInstance().then((prefs) {
      var height = prefs.getDouble('chat_input.keyboard.height');
      if (height != null && height > 0) {
        keyboardHeight = height;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    keyboardVisibilityNotification.removeListener(keyboardListenerId);
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).viewInsets.bottom;
    if (height != null && height > 0) {
      keyboardHeight = height;
      SharedPreferences.getInstance().then((prefs) {
        prefs.setDouble('chat_input.keyboard.height', keyboardHeight);
      });
    }

    return Column(children: <Widget>[
      Container(
          padding: EdgeInsets.only(top: 2, bottom: 2),
          color: Colors.grey[100],
          child: Row(
            children: <Widget>[
              Container(
                  child: IconButton(
                iconSize: 32,
                icon: Icon(inputAction == InputAction.Text
                    ? Icons.radio_button_checked
                    : Icons.keyboard),
                onPressed: onAudioPressed,
              )),
              Expanded(
                child: inputAction == InputAction.Text
                    ? Container(
                        constraints: BoxConstraints.loose(Size.fromHeight(100)),
                        decoration: new BoxDecoration(
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(4.0)),
                            color: Colors.white),
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.all(9)),
                          controller: inputTextController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          maxLines: null,
                          keyboardType: TextInputType.multiline,
                          onChanged: onInputTextChanged,
                          focusNode: textFocusNode,
                        ))
                    : AudioInput(
                        onSubmit: onAudioSubmit,
                      ),
              ),
              Container(
                  width: 60,
                  child: IconButton(
                    iconSize: 32,
                    onPressed: onSubmit,
                    color: submitAction == SubmitAction.Add
                        ? Colors.black
                        : Colors.lightBlue,
                    icon: Icon(submitAction == SubmitAction.Add
                        ? Icons.add_circle_outline
                        : Icons.send),
                  ))
            ],
          )),
      ChatActionPanel(
        visible: actionVisible || keyboardVisible,
        height: keyboardHeight,
      ),
    ]);
  }

  onInputTextChanged(String text) {
    updateSubmitAction(text);
  }

  updateSubmitAction(String text) {
    if (text == null || text.length == 0) {
      submitAction = SubmitAction.Add;
    } else {
      submitAction = SubmitAction.Send;
    }
    setState(() {});
  }

  onAudioPressed() {
    if (inputAction == InputAction.Text) {
      setState(() {
        actionVisible = false;
        inputAction = InputAction.Audio;
        submitAction = SubmitAction.Add;
        FocusScope.of(context).requestFocus(new FocusNode());
      });
    } else {
      setState(() {
        actionVisible = false;
        inputAction = InputAction.Text;
      });
      Future.delayed(Duration(milliseconds: 100)).then((v) {
        FocusScope.of(context).requestFocus(textFocusNode);
      });
    }
  }

  onSubmit() {
    if (submitAction == SubmitAction.Add) {
      setState(() {
        inputAction = InputAction.Text;
        actionVisible = true;
        FocusScope.of(context).requestFocus(new FocusNode());
      });
    } else if (inputTextController.text.length > 0) {
      widget.onSubmit(inputTextController.text);
      inputTextController.clear();
      setState(() {
        submitAction = SubmitAction.Add;
      });
    }
  }

  onAudioSubmit(v) {
    print('send audio');
  }
}
