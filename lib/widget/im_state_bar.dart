import 'package:flutter/material.dart';

enum IMState {
  Connecting,
  Connected,
  Reconnecting,
  Closed,
}

class IMStateBar extends StatefulWidget {
  final IMState imState;
  final Function onRetry;
  IMStateBar({Key key, @required this.imState, @required this.onRetry});
  @override
  IMStateBarState createState() => IMStateBarState();
}

class IMStateBarState extends State<IMStateBar> {
  @override
  Widget build(BuildContext context) {
    Widget root;
    if (widget.imState == IMState.Closed) {
      root = Container(
          color: Colors.redAccent,
          alignment: Alignment.center,
          padding: EdgeInsets.all(6),
          child: Text(
            '网络错误.请检查网络...',
            style: TextStyle(color: Colors.white),
          ));
    } else if (widget.imState == IMState.Reconnecting ||
        widget.imState == IMState.Connecting) {
      root = Container(
          color: Colors.orange,
          alignment: Alignment.center,
          padding: EdgeInsets.all(6),
          child: Text(
            '连接中...',
            style: TextStyle(color: Colors.white),
          ));
    } else {
      root = Container(
        height: 0,
      );
    }
    return GestureDetector(
      onTap: widget.onRetry,
      child: root,
    );
  }
}
