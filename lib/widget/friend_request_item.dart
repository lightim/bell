import 'package:flutter/material.dart';
import 'package:bell/widget/rounded.dart';
import 'package:bell/api/file.dart';
import 'package:bell/api/account.dart';
import 'package:bell/api/friend.dart';
import 'package:bell/db/friend.dart';
import 'package:cached_network_image/cached_network_image.dart';

class FriendRequestItem extends StatefulWidget {
  final int id;
  final int userID;
  final String message;
  final int status;

  FriendRequestItem({this.id, this.userID, this.message, this.status, Key key})
      : super(key: key);

  @override
  FriendRequestItemState createState() => FriendRequestItemState();
}

class FriendRequestItemState extends State<FriendRequestItem> {
  User user;
  bool loading = false;
  int status = FriendRequestStatus.Initial;

  @override
  void initState() {
    super.initState();
    this.status = this.widget.status;
    AccountService.getUser(this.widget.userID).then((u) {
      this.user = u;
      this.setState(() {});
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.all(6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Rounded(
            child: Image(
              width: 48,
              height: 48,
              fit: BoxFit.cover,
              image: CachedNetworkImageProvider(FileService.fileUrl(
                  this.user != null ? this.user.avatar : '')),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.user != null ? this.user.nickname : '',
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(this.widget.message,
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
            ),
          ),
          this.status == FriendRequestStatus.Initial
              ? ButtonTheme(
                  height: 30,
                  minWidth: 20,
                  textTheme: ButtonTextTheme.primary,
                  child: RaisedButton(
                    onPressed: onAcceptFriendRequest,
                    child: Text('添加'),
                  ),
                )
              : (this.status == FriendRequestStatus.Accepted
                  ? Text('已接受')
                  : Text('已拒绝')),
        ],
      ),
    );
  }

  onAcceptFriendRequest() async {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      await FriendService.AcceptFriendRequest(this.widget.id);
      await FriendDatabase.UpdateFriendRequest(
          this.widget.id, FriendRequestStatus.Accepted);
      this.setState(() {
        this.status = FriendRequestStatus.Accepted;
      });
    } catch (e) {
      print(e);
    } finally {
      this.loading = false;
    }
  }
}
