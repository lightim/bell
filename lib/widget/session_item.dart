import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bell/screen/person_chat_screen.dart';
import 'package:bell/widget/rounded.dart';
import 'package:bell/api/file.dart';
import 'package:bell/db/session.dart';
import 'package:bell/api/account.dart';
import 'package:bell/db/user.dart';

class SessionItem extends StatefulWidget {
  Session session;
  SessionItem({this.session, Key key}) : super(key: key);
  @override
  SessionItemState createState() => SessionItemState();
}

class SessionItemState extends State<SessionItem> {
  User user;

  @override
  void initState() {
    super.initState();

    UserDatabase.FetchUser((this.widget.session.tid)).then((u) {
      this.user = u;
      this.setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Container(
            padding: EdgeInsets.only(top: 12, bottom: 12, left: 16, right: 16),
            child: Row(
              children: <Widget>[
                Rounded(
                  child: Image(
                    width: 48,
                    height: 48,
                    fit: BoxFit.cover,
                    image: CachedNetworkImageProvider(FileService.fileUrl(
                        this.user != null ? this.user.avatar : '')),
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      this.user != null ? this.user.nickname : '',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      '呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    )
                  ],
                )),
                Container(
                  height: 48,
                  child: Text(
                    '15:18',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ),
              ],
            )));
  }

  onTap() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonChatScreen(
                  userID: this.user.id,
                )));
  }
}
