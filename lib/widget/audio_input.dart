import 'package:flutter/material.dart';

class AudioInputOverlay extends StatefulWidget {
  AudioInputOverlay({Key key}) : super(key: key);

  @override
  AudioInputOverlayState createState() => AudioInputOverlayState();
}

class AudioInputOverlayState extends State<AudioInputOverlay> {
  static final String textRecording = '手指上滑 取消发送';
  static final String textCancel = '松开手指 取消发送';
  static final Color colorRecording = Colors.transparent;
  static final Color colorCancel = Color(0x99DD1111);

  String text = textRecording;
  Color textColor = colorRecording;

  showRecordingText() {
    setState(() {
      text = textRecording;
      textColor = colorRecording;
    });
  }

  showCancelText() {
    setState(() {
      text = textCancel;
      textColor = colorCancel;
      ;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Container(
            child: Card(
          color: Color(0xAA696969),
          child: Container(
            height: 250,
            width: 250,
            child: Column(
              children: <Widget>[
                Expanded(child: Container()),
                Container(
                    margin: EdgeInsets.only(bottom: 15),
                    padding:
                        EdgeInsets.only(left: 12, top: 6, right: 12, bottom: 6),
                    decoration: BoxDecoration(
                        color: textColor,
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      text,
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ))
              ],
            ),
          ),
        )));
  }
}

enum AudioInputAction {
  None,
  Send,
  Cancel,
}

class AudioInput extends StatefulWidget {
  final ValueChanged<String> onSubmit;

  AudioInput({Key key, @required this.onSubmit}) : super(key: key);

  @override
  AudioInputState createState() => AudioInputState();
}

class AudioInputState extends State<AudioInput> {
  static final String textIdle = '按住 说话';
  static final String textRecording = '松开 结束';
  static final String textCancel = '松开手指，取消发送';

  Color backgroundColor = Colors.white;
  String text = textIdle;
  DateTime actionStart;
  AudioInputAction action = AudioInputAction.None;
  GlobalKey<AudioInputOverlayState> overlayKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onPanDown: onPanDown,
        onPanStart: onPanStart,
        onPanUpdate: onPanUpdate,
        onPanCancel: onPanCancel,
        onPanEnd: onPanEnd,
        child: Container(
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
              color: backgroundColor),
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 5, bottom: 5),
          child: Text(
            text,
            style: TextStyle(fontSize: 18),
          ),
        ));
  }

  onPanDown(v) {
    showRecording();
  }

  onPanStart(v) {
    actionStart = DateTime.now();
    action = AudioInputAction.None;
  }

  onPanUpdate(DragUpdateDetails v) {
    RenderBox box = context.findRenderObject();
    Offset start = box.localToGlobal(Offset.zero);
    Rect rect =
        Rect.fromLTWH(start.dx, start.dy, box.size.width, box.size.height);
    if (v.globalPosition.dy < rect.top - 100) {
      setState(() {
        action = AudioInputAction.Cancel;
        text = textCancel;
      });
      overlayKey.currentState.showCancelText();
    } else {
      setState(() {
        action = AudioInputAction.Send;
        text = textRecording;
      });
      overlayKey.currentState.showRecordingText();
    }
    print('onPanUpdate ${v.globalPosition}');
  }

  onPanCancel() {
    print('onPanCancel');
    showIdle();
  }

  onPanEnd(DragEndDetails v) {
    print('onPanEnd:$v');
    showIdle();
    var now = DateTime.now();
    var diff = now.difference(actionStart);
    if (diff.inSeconds < 1 || action == AudioInputAction.Cancel) {
      return;
    }
    widget.onSubmit('');
  }

  @override
  dispose() {
    super.dispose();
    hideOverlay();
  }

  showRecording() {
    setState(() {
      backgroundColor = Colors.grey[400];
      text = textRecording;
    });
    showOverlay();
  }

  showIdle() {
    setState(() {
      backgroundColor = Colors.white;
      text = textIdle;
    });
    hideOverlay();
  }

  showOverlay() {
    overlayEntry = OverlayEntry(
        builder: (context) => AudioInputOverlay(key: overlayKey),
        opaque: false);
    Overlay.of(context).insert(overlayEntry);
  }

  hideOverlay() {
    if (overlayEntry == null) {
      return;
    }
    overlayEntry.remove();
    overlayEntry = null;
  }

  OverlayEntry overlayEntry;
}
