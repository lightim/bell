import 'package:flutter/material.dart';
import 'package:bell/widget/rounded.dart';

class Badge extends StatelessWidget {
  final int count;
  Badge({Key key, @required this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var text = count.toString();
    if (count > 99) {
      text = '99+';
    }
    return Rounded(
        radius: 20,
        child: Container(
          padding: EdgeInsets.all(3),
          alignment: Alignment.center,
          constraints: BoxConstraints(
            minHeight: 20.0,
            minWidth: 20.0,
          ),
          color: Colors.red,
          child:
              Text(text, style: TextStyle(color: Colors.white, fontSize: 12)),
        ));
  }
}
