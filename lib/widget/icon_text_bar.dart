import 'package:flutter/material.dart';
import 'package:bell/widget/badge.dart';

class IconTextBar extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final String text;
  final GestureTapCallback onTap;
  final int badge;

  IconTextBar(
      {Key key,
      this.icon,
      this.text,
      this.onTap,
      this.iconColor,
      this.badge = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
              child: Row(
                children: <Widget>[
                  icon != null
                      ? Icon(icon, size: 34, color: iconColor)
                      : Container(height: 34),
                  Expanded(
                    child: Container(
                      margin: icon != null ? EdgeInsets.only(left: 16) : null,
                      child: Text(text, style: TextStyle(fontSize: 18)),
                    ),
                  ),
                  (badge != null && badge > 0)
                      ? Badge(count: badge)
                      : Container(),
                ],
              ),
            )));
  }
}

class IconTextDivider extends StatelessWidget {
  final double left;
  IconTextDivider({Key key, this.left = 64}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: left),
      child: Divider(
        height: 1,
      ),
    );
  }
}
