import 'package:flutter/material.dart';

class Rounded extends StatelessWidget {
  final Widget child;
  final double radius;

  Rounded({Key key, this.child, this.radius = 6}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(borderRadius: BorderRadius.circular(radius), child: child);
  }
}
