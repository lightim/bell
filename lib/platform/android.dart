import 'package:flutter/services.dart';

class AndroidChannel {
  //初始化通信管道-设置退出到手机桌面
  static const String CHANNEL = "android/channel";

  //设置回退到手机桌面
  static Future<bool> moveToBack() async {
    final platform = MethodChannel(CHANNEL);
    //通知安卓返回,到手机桌面
    try {
      await platform.invokeMethod('moveToBack');
      return true;
    } on PlatformException catch (e) {}
    return false;
  }
}
