import 'package:bell/db/sqlite3.dart';
import 'package:bell/api/friend.dart';

class FriendDatabase {
  static Future<void> InsertFriendRequest(FriendRequest req) async {
    var db = await getDatabase();
    await db.rawInsert(
        "INSERT OR IGNORE INTO friend_request(id,user_id,friend_id,remark,message,status,utime,ctime) VALUES(?,?,?,?,?,?,?,?)",
        [
          req.id,
          req.userID,
          req.friendID,
          req.remark,
          req.message,
          req.status,
          req.utime.toString(),
          req.ctime.toString(),
        ]);
  }

  static Future<void> UpdateFriendRequest(int id, int status) async {
    var db = await getDatabase();
    await db.rawUpdate(
        "UPDATE friend_request SET status=? WHERE id=?", [status, id]);
  }

  static Future<List<FriendRequest>> FindFriendRequests() async {
    var db = await getDatabase();
    var res = await db.query("friend_request", orderBy: "ctime DESC");
    List<FriendRequest> list = res.isNotEmpty
        ? res.map((c) => FriendRequest.fromJson(c)).toList()
        : [];
    return list;
  }
}
