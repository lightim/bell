import 'package:bell/db/sqlite3.dart';
import 'package:bell/api/account.dart';

class SessionType {
  static final int Person = 0;
  static final int Group = 1;
}

class Session {
  final int type;
  final int tid;
  final DateTime utime;
  final DateTime ctime;

  Session({this.type, this.tid, this.ctime, this.utime});

  factory Session.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Session(
        type: json['type'],
        tid: json['tid'],
        ctime: DateTime.tryParse(json['ctime']),
        utime: DateTime.tryParse(json['utime']));
  }
}

class SessionDatabase {
  static Future<void> InsertSession(int type, int tid) async {
    var db = await getDatabase();
    await db.rawInsert(
        "REPLACE INTO session(type,tid,utime,ctime) VALUES(?,?,?,?)", [
      type,
      tid,
      new DateTime.now().toIso8601String(),
      new DateTime.now().toIso8601String()
    ]);
  }

  static Future<List<Session>> FindSessions() async {
    var db = await getDatabase();
    var res = await db.query("session", orderBy: "utime DESC");
    List<Session> list =
        res.isNotEmpty ? res.map((c) => Session.fromJson(c)).toList() : [];
    return list;
  }
}
