import 'package:bell/db/sqlite3.dart';
import 'package:bell/light/message.pb.dart' as pb;
import 'dart:convert' as JSON;

class Message {
  final String id;
  final int version;
  final int fromUserID;
  final int toUserID;
  final int toGroupID;
  final int toRoomID;
  final int type;
  final int status;
  final pb.ContentMessage content;
  final pb.Notification notification;
  final DateTime ctime;
  Message(
      {this.id,
      this.fromUserID,
      this.version,
      this.toUserID,
      this.toGroupID,
      this.toRoomID,
      this.type,
      this.status,
      this.content,
      this.notification,
      this.ctime});

  factory Message.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Message(
      type: json['type'],
      id: json['id'],
      version: json['version'],
      fromUserID: json['from_user_id'],
      toUserID: json['to_user_id'],
      toGroupID: json['to_group_id'],
      toRoomID: json['to_room_id'],
      status: json['status'],
      content: pb.ContentMessage.fromJson(json['content']),
      notification: pb.Notification.fromJson(json['notification']),
      ctime: DateTime.tryParse(json['ctime']),
    );
  }
  factory Message.fromPB(pb.Message pm, int status) {
    if (pm == null) {
      return null;
    }
    return Message(
        type: pm.type.value,
        id: pm.id,
        ctime: DateTime.fromMillisecondsSinceEpoch(
            (pm.ctime.toInt() / 1000000).round()),
        version: pm.version.value,
        fromUserID: int.tryParse(pm.fromUserId) ?? 0,
        toUserID: int.tryParse(pm.toUserId) ?? 0,
        toGroupID: int.tryParse(pm.toGroupId) ?? 0,
        toRoomID: int.tryParse(pm.toRoomId) ?? 0,
        status: status,
        content: pm.contentMessage,
        notification: pm.notification);
  }
}

class MessageDatabase {
  static Future<void> InsertMessage(Message m) async {
    var db = await getDatabase();
    await db.execute(
        "INSERT INTO message(id,version,from_user_id,to_user_id,to_group_id,to_room_id,type,status,content,notification,ctime) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
        [
          m.id,
          m.version,
          m.fromUserID,
          m.toUserID,
          m.toGroupID,
          m.toRoomID,
          m.type,
          m.status,
          m.content.writeToJson(),
          m.notification.writeToJson(),
          m.ctime.toString(),
        ]);
  }

  static Future<List<Message>> FindPersonMessage(
      int userID, DateTime t, int limit) async {
    var db = await getDatabase();
    var res = await db.query("message",
        orderBy: "ctime",
        where: "type=? AND (from_user_id=? OR to_user_id=?) AND ctime<?",
        whereArgs: [
          pb.Message_Type.ContentMessage.value,
          userID,
          userID,
          t.toString()
        ],
        limit: limit);
    List<Message> list =
        res.isNotEmpty ? res.map((c) => Message.fromJson(c)).toList() : [];
    return list;
  }
}
