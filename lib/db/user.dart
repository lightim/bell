import 'package:bell/db/sqlite3.dart';
import 'package:bell/api/account.dart';

class UserDatabase {
  static Future<void> InsertUser(Account u) async {
    var db = await getDatabase();
    await db.rawInsert(
        "REPLACE INTO user(id,email,username,nickname,avatar,gender,utime,ctime) VALUES(?,?,?,?,?,?,?,?) ",
        [
          u.id,
          u.email,
          u.username,
          u.nickname,
          u.avatar,
          u.gender,
          u.utime.toString(),
          u.ctime.toString(),
        ]);
  }

  static Future<User> GetUser(int id) async {
    var db = await getDatabase();
    var res = await db.query("user", where: "id=" + id.toString());
    if (res.isEmpty) {
      return null;
    }
    return User.fromJson(res[0]);
  }

  static Future<User> FetchUser(int id) async {
    try {
      var u = await AccountService.getUser(id);
      if (u != null) {
        InsertUser(u);
        return u;
      }
    } catch (e) {
      return GetUser(id);
    }
  }
}
