import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:bell/api/api.dart';

initDB() async {
  var userID = await getUserID();
  Directory documentsDirectory = await getApplicationDocumentsDirectory();
  String path =
      join(documentsDirectory.path, 'bell.' + userID.toString() + '.db');
  return await openDatabase(path, version: 1, onOpen: (db) {},
      onCreate: (Database db, int version) async {
    await db.execute("CREATE TABLE friend_request ("
        "id INTEGER PRIMARY KEY,"
        "user_id INTEGER,"
        "friend_id INTEGER,"
        "remark TEXT DEFAULT '',"
        "message TEXT DEFAULT '',"
        "status INTEGER DEFAULT 0,"
        "ctime TIMESTAMP WITH TIMEZONE,"
        "utime TIMESTAMP WITH TIMEZONE"
        ")");
    await db.execute("CREATE TABLE user("
        "id INTEGER PRIMARY KEY,"
        "email TEXT NOT NULL DEFAULT '',"
        "username TEXT NOT NULL DEFAULT '',"
        "nickname TEXT NOT NULL DEFAULT '',"
        "avatar TEXT NOT NULL DEFAULT '',"
        "gender INTEGER NOT NULL DEFAULT 0,"
        "ctime TIMESTAMP WITH TIMEZONE,"
        "utime TIMESTAMP WITH TIMEZONE"
        ")");
    await db.execute("CREATE TABLE session("
        "type INTEGER NOT NULL DEFAULT 0,"
        "tid INTEGER NOT NULL,"
        "ctime TIMESTAMP WITH TIMEZONE,"
        "utime TIMESTAMP WITH TIMEZONE,"
        "UNIQUE(type,tid) ON CONFLICT REPLACE)");
    await db.execute("CREATE TABLE message("
        "id TEXT NOT NULL PRIMARY KEY,"
        "version INTEGER NOT NULL,"
        "from_user_id INTEGER NOT NULL DEFAULT 0,"
        "to_user_id INTEGER NOT NULL DEFAULT 0,"
        "to_group_id INTEGER NOT NULL DEFAULT 0,"
        "to_room_id INTEGER NOT NULL DEFAULT 0,"
        "type INTEGER NOT NULL,"
        "status INTEGER NOT NULL DEFAULT 0,"
        "content TEXT NOT NULL DEFAULT '',"
        "notification TEXT NOT NULL DEFAULT '',"
        "ctime TIMESTAMP WITH TIMEZONE)");
  });
}

clearDB() async {
  // if (_db == null) {
  //   return;
  // }
  // await _db.execute("DELETE FROM friend_request");
}

Database _db;

Future<Database> getDatabase() async {
  if (_db == null) {
    _db = await initDB();
  }
  return _db;
}
