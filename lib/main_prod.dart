import 'package:flutter/material.dart';
import 'package:bell/app.dart';
import 'package:bell/app_config.dart';

void main() {
  AppConfig configuredApp = AppConfig(
      appName: 'Bell',
      apiBaseUrl: 'https://bell.wikylyu.xyz/api',
      imBaseUrl: 'wss://bell.wikylyu.xyz/light',
      imAppId: 'q1EqRL0xF884',
      debug: false,
      child: App());
  runApp(configuredApp);
}
