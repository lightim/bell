import 'package:bell/light/client.dart';
import 'package:bell/light/message.pb.dart';
import 'package:bell/app_config.dart';

typedef Future<void> IMEventHandler();
typedef Future<void> IMMessageHandler(Message m);
typedef Future<void> IMErrorHandler(IMErrorCode errcode);

class IMManager {
  static LightClient client;
  static String token;

  /* 同一个事件允许注册多个，按照先后顺序执行 */
  static List<IMEventHandler> onopens = [];
  static List<IMMessageHandler> onmessages = [];
  static List<IMEventHandler> oncloses = [];
  static List<IMEventHandler> onreconnects = [];
  static List<IMErrorHandler> onerrors = [];

  static int retryMax = 3;
  static int retryN = retryMax;

  static void addonopen(Function _onopen) {
    onopens.add(_onopen);
  }

  static void rmonopen(Function _onopen) {
    for (var i = 0; i < onopens.length; i++) {
      if (onopens[i] == _onopen) {
        onopens.removeAt(i);
        break;
      }
    }
  }

  static addonmessage(Function _onmessage) {
    onmessages.add(_onmessage);
  }

  static rmonmessage(Function _onmessage) {
    for (var i = 0; i < onmessages.length; i++) {
      if (onmessages[i] == _onmessage) {
        onmessages.removeAt(i);
        break;
      }
    }
  }

  static void addonclose(Function _onclose) {
    oncloses.add(_onclose);
  }

  static void addonerror(Function _onerror) {
    onerrors.add(_onerror);
  }

  static void rmonerror(Function _onerror) {
    for (var i = 0; i < onerrors.length; i++) {
      if (onerrors[i] == _onerror) {
        onerrors.removeAt(i);
        break;
      }
    }
  }

  static void rmonclose(Function _onclose) {
    for (var i = 0; i < oncloses.length; i++) {
      if (oncloses[i] == _onclose) {
        oncloses.removeAt(i);
        break;
      }
    }
  }

  static void addonreconnect(Function _onreconnect) {
    onreconnects.add(_onreconnect);
  }

  static void rmonreconnect(Function _onreconnect) {
    for (var i = 0; i < onreconnects.length; i++) {
      if (onreconnects[i] == _onreconnect) {
        onreconnects.removeAt(i);
        break;
      }
    }
  }

  static Future<void> connect(String t) async {
    if (t == null || t.length == 0) {
      onerror(IMErrorCode.AuthFailed);
      return;
    }
    if (client != null) {
      client.close();
      client = null;
    }
    retryN = retryMax;
    token = t;
    var appConfig = AppConfig.instance;
    try {
      client = await LightClient.connect(appConfig.imBaseUrl, onopen, onmessage,
          () async {
        client = null;
        await _reconnect();
      }, onerror, appConfig.imAppId, token);
    } catch (e) {
      await _reconnect();
    }
  }

  static Future<void> _reconnect() async {
    var appConfig = AppConfig.instance;
    if (retryN < 0) {
      onclose();
      return;
    }
    onreconnect();
    /* 延时三秒 */
    print('reconnecting in 2 seconds...');
    await Future.delayed(Duration(seconds: 2), () => true);
    if (retryN < 0) {
      print('reconnecting cancelled.');
      onclose();
      return;
    }
    print('reconnecting...');

    retryN--;
    try {
      client = await LightClient.connect(appConfig.imBaseUrl, onopen, onmessage,
          () async {
        await _reconnect();
      }, onerror, appConfig.imAppId, token);
      retryN = 3;
    } catch (e) {
      await _reconnect();
    }
  }

  /* 断开链接 */
  static void disconnect() {
    if (client == null) {
      return;
    }
    /* 防止重连 */
    retryN = -1;
    client.close();
    client = null;
    token = '';
    onclose();
  }

  static bool send(Message m) {
    if (client == null || client.status != IMStatus.Authed) {
      return false;
    }
    client.send(m);
    return true;
  }

  static void onmessage(Message m) async {
    for (var i = 0; i < onmessages.length; i++) {
      await onmessages[i](m);
    }
  }

  static void onclose() async {
    for (var i = 0; i < oncloses.length; i++) {
      await oncloses[i]();
    }
  }

  /* 链接服务器且认证成功 */
  static void onopen() async {
    for (var i = 0; i < onopens.length; i++) {
      await onopens[i]();
    }
  }

  static void onerror(errcode) async {
    for (var i = 0; i < onerrors.length; i++) {
      await onerrors[i](errcode);
    }
  }

  static void onreconnect() async {
    for (var i = 0; i < onreconnects.length; i++) {
      await onreconnects[i]();
    }
  }
}
