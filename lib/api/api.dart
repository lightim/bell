import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:bell/app_config.dart';

Future<void> setToken(String t, int uesrID) async {
  var prefs = await SharedPreferences.getInstance();
  await prefs.setString('token', t);
  await prefs.setInt("userID", uesrID);
}

Future<String> getToken() async {
  var prefs = await SharedPreferences.getInstance();
  var t = prefs.getString('token');
  if (t == null || t.length == 0) {
    return '';
  }
  return t;
}

Future<int> getUserID() async {
  var prefs = await SharedPreferences.getInstance();
  var t = prefs.getInt('userID');
  if (t == null) {
    return 0;
  }
  return t;
}

Future<void> clearToken() async {
  var prefs = await SharedPreferences.getInstance();
  await prefs.clear();
}

Map<String, String> defaultHeaders = {
  'Content-type': 'application/json;charset=UTF-8',
};

Future<dynamic> put(String path, Map body, {String token = ''}) async {
  var url = AppConfig.instance.apiBaseUrl + path;
  var headers = Map<String, String>.from(defaultHeaders);
  headers['X-Authorization'] = token;
  var r = await http.put(url, body: json.encode(body), headers: headers);
  var rbody = json.decode(r.body);
  return rbody;
}

Future<dynamic> post(String path, Map body, {String token = ''}) async {
  var url = AppConfig.instance.apiBaseUrl + path;
  var headers = Map<String, String>.from(defaultHeaders);
  headers['X-Authorization'] = token;
  var r = await http.post(url, body: json.encode(body), headers: headers);
  var rbody = json.decode(r.body);
  return rbody;
}

Future<dynamic> get(String path, {String token = ''}) async {
  var url = AppConfig.instance.apiBaseUrl + path;
  var headers = Map<String, String>.from(defaultHeaders);
  headers['X-Authorization'] = token;
  var r = await http.get(url, headers: headers);
  var rbody = json.decode(r.body);
  return rbody;
}

class ApiException implements Exception {
  final int status;
  ApiException({this.status});
}

Future<dynamic> postStream(String path, String fieldName, String filename,
    http.ByteStream stream, int length) async {
  var url = Uri.parse(AppConfig.instance.apiBaseUrl + path);

  var request = new http.MultipartRequest("POST", url);
  var multipartFile =
      new http.MultipartFile(fieldName, stream, length, filename: filename);

  request.files.add(multipartFile);
  var r = await http.Response.fromStream(await request.send());
  var rbody = json.decode(r.body);
  return rbody;
}
