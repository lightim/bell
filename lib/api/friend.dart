import 'dart:convert';
import 'package:bell/api/api.dart' as api;
import 'package:bell/api/account.dart';

class FriendRequestStatus {
  static final int Initial = 0;
  static final int Denied = 1;
  static final int Accepted = 2;
}

class FriendRequest {
  final int id;
  final int userID;
  final int friendID;
  final String message;
  final String remark;
  final int status;
  final DateTime utime;
  final DateTime ctime;
  final Account user;
  FriendRequest(
      {this.id,
      this.userID,
      this.friendID,
      this.message,
      this.remark,
      this.status,
      this.ctime,
      this.utime,
      this.user});

  factory FriendRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return FriendRequest(
        id: json['id'],
        userID: json['user_id'],
        friendID: json['friend_id'],
        message: json['message'],
        remark: json['remark'],
        status: json['status'],
        ctime: DateTime.tryParse(json['ctime']),
        utime: DateTime.tryParse(json['utime']),
        user: Account.fromJson(json['user']));
  }
}

class UserFriend {
  final int id;
  final int userID;
  final int friendID;
  final DateTime utime;
  final DateTime ctime;
  final Account friend;
  UserFriend(
      {this.id,
      this.userID,
      this.friendID,
      this.ctime,
      this.utime,
      this.friend});

  factory UserFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return UserFriend(
        id: json['id'],
        userID: json['user_id'],
        friendID: json['friend_id'],
        ctime: DateTime.tryParse(json['ctime']),
        utime: DateTime.tryParse(json['utime']),
        friend: Account.fromJson(json['friend']));
  }
}

class FriendService {
  static Future<FriendRequest> CreateFriendRequest(
      int userID, String message, String remark) async {
    var token = await api.getToken();
    var body = await api.post(
        "/friend/request",
        {
          'user_id': userID,
          'message': message,
          'remark': remark,
        },
        token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    var u = FriendRequest.fromJson(body['data']);
    return u;
  }

  static Future<FriendRequest> GetFriendRequest(int id) async {
    var token = await api.getToken();
    var body = await api.get("/friend/request/" + id.toString(), token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    var u = FriendRequest.fromJson(body['data']);
    return u;
  }

  static Future<void> AcceptFriendRequest(int id) async {
    var token = await api.getToken();
    var body =
        await api.put("/friend/request/accept", {'id': id}, token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    return;
  }

  static Future<List<UserFriend>> FindUserFriends() async {
    var token = await api.getToken();
    var body = await api.get('/friends', token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    List<UserFriend> list = [];
    for (var i in body['data']) {
      list.add(UserFriend.fromJson(i));
    }
    return list;
  }
}
