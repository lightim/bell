import 'package:path/path.dart';
import 'package:http/http.dart';
import 'package:async/async.dart';
import 'dart:convert';
import 'dart:io';
import 'package:bell/app_config.dart';
import 'package:bell/api/api.dart' as api;

class FileService {
  static Future<String> upload(File source) async {
    var stream = ByteStream(DelegatingStream.typed(source.openRead()));
    var length = await source.length();
    var body = await api.postStream(
        '/file', 'file', basename(source.path), stream, length);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    return body['data']['fileid'];
  }

  static String fileUrl(String fileid) {
    return AppConfig.instance.apiBaseUrl + '/file/' + fileid;
  }
}
