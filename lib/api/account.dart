import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:bell/api/api.dart' as api;

class Token {
  final int userID;
  final String token;
  final DateTime etime;
  final DateTime utime;
  final DateTime ctime;

  Token({this.userID, this.token, this.etime, this.utime, this.ctime});

  factory Token.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Token(
        userID: json['user_id'],
        token: json['token'],
        etime: DateTime.tryParse(json['etime']),
        utime: DateTime.tryParse(json['etime']),
        ctime: DateTime.tryParse(json['etime']));
  }

  String toJson() {
    return json.encode({
      'user_id': userID,
      'token': token,
      'etime': etime != null ? etime.toIso8601String() : '',
      'ctime': ctime != null ? ctime.toIso8601String() : '',
      'utime': utime != null ? utime.toIso8601String() : '',
    });
  }
}

class Account {
  final int id;
  final String email;
  final String username;
  final String nickname;
  final String avatar;
  final int gender;
  final DateTime utime;
  final DateTime ctime;

  Account(
      {this.id,
      this.email,
      this.username,
      this.nickname,
      this.avatar,
      this.gender,
      this.ctime,
      this.utime});

  factory Account.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Account(
        id: json['id'],
        email: json['email'],
        username: json['username'],
        nickname: json['nickname'],
        avatar: json['avatar'],
        gender: json['gender'],
        ctime: DateTime.tryParse(json['ctime']),
        utime: DateTime.tryParse(json['utime']));
  }

  String toJson() {
    return json.encode({
      'id': id,
      'email': email,
      'username': username,
      'nickname': nickname,
      'avatar': avatar,
      'gender': gender,
      'ctime': ctime != null ? ctime.toIso8601String() : '',
      'utime': utime != null ? utime.toIso8601String() : '',
    });
  }
}

class UserRelationship {
  final bool isFriend;
  UserRelationship({this.isFriend});

  factory UserRelationship.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return UserRelationship(isFriend: json['is_friend']);
  }
}

class User extends Account {
  UserRelationship relationship;
  User(int id, String email, String username, String nickname, String avatar,
      int gender, DateTime utime, DateTime ctime, {this.relationship})
      : super(
            id: id,
            email: email,
            username: username,
            nickname: nickname,
            avatar: avatar,
            gender: gender,
            utime: utime,
            ctime: ctime);

  factory User.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return User(
        json['id'],
        json['email'],
        json['username'],
        json['nickname'],
        json['avatar'],
        json['gender'],
        DateTime.tryParse(json['utime']),
        DateTime.tryParse(json['ctime']),
        relationship: UserRelationship.fromJson(json['relationship']));
  }
}

class AccountService {
  static Future<Token> login(String username, String password) async {
    Map payload = {
      'username': username,
      'password': password,
    };
    var body = await api.put('/account', payload);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    return Token.fromJson(body['data']);
  }

  static Future<Token> signup(
      String email, String password, String avatar, String nickname) async {
    Map payload = {
      'email': email,
      'password': password,
      'avatar': avatar,
      'nickname': nickname,
    };
    var body = await api.post('/account', payload);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    return Token.fromJson(body['data']);
  }

  static Future<Account> getAccount() async {
    var token = await api.getToken();
    var body = await api.get('/account', token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    var account = Account.fromJson(body['data']);
    if (account != null) {
      setCachedAccount(account);
    }
    return account;
  }

  static Future<Account> getUser(int id) async {
    var token = await api.getToken();
    var body = await api.get("/user?id=" + id.toString(), token: token);
    if (body['status'] == 10003) {
      return null;
    } else if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    var u = User.fromJson(body['data']);
    return u;
  }

  static Future<void> setCachedAccount(Account account) async {
    var prefs = await SharedPreferences.getInstance();
    var data = account.toJson();
    debugPrint('setAccount:$data');
    await prefs.setString('account', data);
  }

  static Future<Account> getCachedAccount() async {
    var prefs = await SharedPreferences.getInstance();
    var data = prefs.getString('account');
    if (data == null || data == '') {
      return null;
    }
    debugPrint('getAccount:$data');

    var account = Account.fromJson(json.decode(data));
    if (account == null) {
      account = await getAccount();
    }
    return account;
  }

  static Future<List<Account>> searchAccount(String q) async {
    var token = await api.getToken();
    var body = await api.get('/account/search?q=' + Uri.encodeQueryComponent(q),
        token: token);
    if (body['status'] != 0) {
      throw api.ApiException(status: body['status']);
    }
    List<Account> result = [];
    for (var d in body['data']) {
      result.add(Account.fromJson(d));
    }
    return result;
  }
}
