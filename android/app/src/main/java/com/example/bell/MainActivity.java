package com.example.bell;

import android.os.Bundle;
import android.view.KeyEvent;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    // 通讯名称,回到手机桌面
    private final String CHANNEL = "android/channel";
    // 1.是否返回到手机桌面，不退出app，
    // 2.不做处理,默认不做处理
    private boolean out = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                // 通过methodCall可以获取参数和方法名 执行对应的平台业务逻辑即可
                if (methodCall.method.equals("moveToBack")) {// 设置 当前flutter页退出到手机桌面
                    moveToBack();
                }
            }
        });
    }

    // 是否返回到手机桌面
    private void moveToBack() {
        moveTaskToBack(true);
    }

}
